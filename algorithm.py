class NaiveBayes:
    def __init__(self, targetAtt, datasetObj):
        """
            Constructor of the class that initializes the variables.
            
            Parameter:
                targetAtt: (string) target attribute 
        """
        self.classAttr = targetAtt
        self.datasetObj = datasetObj
        self.columns = list(datasetObj.getColumnHeaders())
        self.classIndex = self.columns.index(self.classAttr)
        self.training_dataset = datasetObj.getRecords()
        self.datastruc = []  # self.__createDataStructure()
        
    def __prior(self):
        """
            Calculates the probability of target class, also called prior.
            
            Finds distinct values in target attributes and find occurrences of
            each of the distinct value and calculates the probability. It then 
            adds calculated probability for given value in data structure.
            
        """
        # get distinct value for given attribute
        distinct_values = self.__getDistinctValues(self.classIndex)
        # iterate over each distinct value of attribute 
        frequency = {}
        for val in distinct_values:
            frequency[val] = 0
        # count occurences of the value in training dataset
        for row in self.training_dataset:
            rval = row[self.classIndex]
            frequency[rval] += 1
        
        total_rows = self.datasetObj.getRowsCount()
        # calculate the probability
        for key in frequency.keys():
            probability = frequency.get(key) / float(total_rows)
            # add dictionary in data structure
            self.datastruc.append({self.classAttr:{key:probability}})
            
    def __likelihood(self):
        """
            Calculates the probability of distinct values in attribute (column) for
            each prior classes and stores the result in data struc.
            
        """
        # for each class value in datastructure
        for classDict in self.datastruc:  
            # get values of class attribute
            # store attribute's values' probabilities
            attrs_list = []
            for class_value in classDict[self.classAttr].keys():
                # iterate over all columns
                for attr_idx in range(len(self.columns)):
                    # spare class index
                    if attr_idx == self.classIndex:
                        continue
                    # get attribute of attr_idx
                    attribute = self.columns[attr_idx]
                    # get distinct values of attribute
                    distinct = self.__getDistinctValues(attr_idx)
                    # store probabilities of attribute's values
                    attr_dict = {}
                    # intialize each distinct value to zero
                    for val in distinct:
                        attr_dict[val] = 0
                    
                    # find probability of each distinct value
                    total_rows = 0
                    for row in self.training_dataset:
                        count = row[self.classIndex].count(class_value)
                        if count > 0:
                            total_rows += count 
                            attr_dict[row[attr_idx]] += 1
                    for key in attr_dict.keys():                        
                        probability = attr_dict.get(key) / float(total_rows)
                        if probability == 0.0:
                            dist_len = float(len(distinct))
                            probability = (attr_dict.get(key) + (1 * (1 / dist_len))) / (float(total_rows) + 1)
                        # append attribute with value's probabilities in list
                        attr_dict[key] = probability
                    attrs_list.append({attribute:attr_dict})
                # append back list in classDict
            classDict['attrs'] = attrs_list

    def __predictTestData(self, testdbObj):
        """
            This function is invoked to predict test data. It iterates over each
            record in test data and finds bayesian classification for each class of
            the target attribute and predicts the classification.
            
            Parameters:
                testdbObj: (object) object of class Dataset of module dataset.py
            
            Return:
                tuple: returns array of actual classification and prediction in tuple 
        """
        test_columns = testdbObj.getColumnHeaders()
        pred_classification = []
        actual_classification = []
        # get records and iterate over test dataset
        for row in testdbObj.getRecords():
            classification_dict, classification, cmax = {}, "", 0.0
            actual_classification.append(row[self.classIndex])
            # iterate over data structure and get each dictionary of the class
            for item_dict in self.datastruc:
                attrs_list = item_dict.get('attrs')
                #classification_prob = 1   
                #prior_key = None
                prior_key = item_dict.get(self.classAttr).keys()[0]
                prior_prob = item_dict.get(self.classAttr).get(prior_key)
                classification_prob = prior_prob          
                # iterate for each column in row
                for i in range(len(row)):
                    # if column is class column then get prob from datastructure
                    if i == self.classIndex:
                        continue

                    for adict in attrs_list:
                        ds_attribute_dict = adict.get(test_columns[i])
                        if ds_attribute_dict is not None: 
                            classification_prob *= ds_attribute_dict.get(row[i]) 
                            break
                classification_dict[prior_key] = classification_prob
            # find which has the max scoree and select that class's value as classification 
            for key, value in classification_dict.iteritems():
                if cmax < value:
                    cmax = value
                    classification = key
            pred_classification.append(classification)
        print "Prediction: " + str(pred_classification)
        return actual_classification, pred_classification            

    def __getAccuracy(self, actual_classification=[], pred_classification=[]):
        """
            This function calculates the accuracy in percentage.
            
            Parameters:
                actual_classification: (list) actual classification from test data
                pred_classification: (list) predicted classification for test data
            
            Return:
                accuracy: (float) calculated accuracy.
        """
        total = len(actual_classification)
        count = 0
        for i in range(total):
            if actual_classification[i] == pred_classification[i]:
                count += 1
        accuracy = (float(count) / total) * 100
        return accuracy                
    
    def __getDistinctValues(self, attrIndex):
        distinct = set()
        for row in self.training_dataset:
            distinct.add(row[attrIndex])
        return list(distinct)
    
    def __createDataStructure(self):
        """
            Datastructure:
              
                [
                    {    
                        class: {class1val: prob}, 
                        attrs:[
                            attr1: {attr1val1: prob, attr1val2: prob},
                            attr2: {attr1val1: prob, attr1val2: prob}]
                    }
                ]
        """
        self.__prior()
        self.__likelihood()
    
    def run(self, testdbObj):
        self.__createDataStructure()
        print "Data structure:\n"
        print self.datastruc
        classifications = self.__predictTestData(testdbObj)
        accuracy = self.__getAccuracy(classifications[0], classifications[1])
        testdbObj.appendColumnToDataset(classifications[1])
        return accuracy
