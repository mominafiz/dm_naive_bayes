# script file to run the algorithm.
from dataset import Dataset
from ui import userInterface, targetClass
from algorithm import NaiveBayes

userinput_tuple = userInterface()
#userinput_tuple = ('data1','data1', 'data1.o2')
# create dataset object.
traindbObj = Dataset(userinput_tuple[0])
# get eligible target classes
columns = traindbObj.getColumnHeaders()
# calls function to ask user to select target class with binary variable
targetclass = targetClass(columns)
# get index of target class
target_index = columns.index(targetclass)
# create object of algorithm and inject dependency
algoObj = NaiveBayes(targetclass, traindbObj)
testdbObj = Dataset(userinput_tuple[1])
accuracy = algoObj.run(testdbObj)
testdbObj.writePredictionToFile(userinput_tuple[2], accuracy)

print("\nOutput written to file. Please check file under data folder of project root.")
print("------------------    End of the Program    -------------------------------")
