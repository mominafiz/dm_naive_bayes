
class Dataset:
    def __init__(self,filename):
        """
            Constructor to initialize variables.
        """
        self.__columns = ()
        self.__records = []
        self.__lines = None
        self.__rows = 0
        if filename is not None or filename != "":
            self.loadDataset(filename)
    
    def __createColumnHeaders(self):
        """
            private function to create headers tuple from read file. 
            This function assumes first line is header file and stores
            in object's variable.
        """
        if len(self.__lines) >= 1:
            columnstr = self.__lines[0]
            self.__columns = columnstr.lower().split()
            
    def __createRecords(self):
        """
            Private function creates records, except header row and store row as tuple in list.
        """
        if len(self.__lines) < 1:
            raise Exception("No records in file.")
        # slice after 1st elemet in the list and store it in list  after
        # dividing it in columns.
        for line in self.__lines[1:]:
            if len(line.strip()) < 1:
                continue
            self.__records.append(line.lower().split())
            
    def __validateAndCleanRecords(self):
        """
            Private function used to validate number of elements in each row
            is equal to number of elements of header row.
        """ 
        for record in self.__records:
            if len(record) != len(self.__columns):
                raise Exception("Column and record length mismatch.")
            for value in record:
                if value is None or value == "":
                    del record
            
    def loadDataset(self, filename=""):
        """
            This function is invoke to load training dataset, then it invokes private
            functions to create headers and records, and finally validates data.
             
            This function accepts filename as the input parameter. It opens
            the file and read the contents.
            
            Parameter:
                filename: (string) accepts filename to read the dataset
        """
        try:
            # open file in read mode
            dbfile = open("data/" + filename)
            # read all content of the file, each row as string and store in list
            self.__lines = dbfile.readlines()
            # call to private function to create column header  
            self.__createColumnHeaders()
            # call to private function to create rows of data except header.
            self.__createRecords()
            # validation
            self.__validateAndCleanRecords()
            # save count of rows in dataset to avoid additional computation
            self.__rows = len(self.__records)
        except IOError:
            print("File not found!")
        except Exception as exp:
            print("Error! Unexpected error occurred while loading dataset. " + exp)
    
    def writePredictionToFile(self, filename, accuracy):
        """
            Invoke this function to write rule to output stream. Output is 
            stored under data folder of project root.
            
            Parameter:
                filename: (string) output filename
                rules: (string) output buffer rules to be written
        """
        writefile = None
        try:
            col_width_list = self.getColumnWidth()
            writefile = open("data/" + filename, 'w')
            if writefile is None:
                raise Exception("File cannot be opened.")
            line = ""
            for colidx in range(len(self.__columns)):
                total_space = col_width_list[colidx] - len(self.__columns[colidx]) + 1
                line += self.__columns[colidx]
                for i in range(total_space):
                    line += ' '
            writefile.write(line + "\n")
            
            for row in self.__records:
                line = ""
                for idx in range(len(row)):
                    total_space = col_width_list[idx] - len(row[idx]) + 1
                    line += row[idx]
                    for i in range(total_space):
                        line += ' '
                writefile.write(line + "\n")
            writefile.write("Accuracy:    {0}%".format(str(round(accuracy,2))))
        except Exception as exp:
            print(str(exp) + "Error occurred while writing rules.")
        finally:
            if writefile is not None:
                writefile.close()
    
    def getColumnWidth(self):
        max = []
        for index in range(len(self.__columns)):
            max.append(0)
            if max[index] < len(self.__columns[index]):
                max[index] = len(self.__columns[index])
        
        for row in self.__records:
            for index in range(len(row)):
                if max[index] < len(row[index]):
                    max[index] = len(row[index])
        return max
                
     
    def appendColumnToDataset(self, column=[]):
        self.__columns.append('classification')
        for i in range(self.getRowsCount()):
            self.__records[i].append(column[i])        
        
    def getRecords(self):
        return self.__records
    
    def getRowsCount(self):
        return self.__rows
    
    def getColumnHeaders(self):
        return self.__columns
    
