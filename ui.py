# implement UI for data file input, target attribute.

def userInterface():
    """
        This function is the implementation for User Interface.
        User enters training file name and test filename.
        
        Return:
            (tuple): training and test data filename 
    """
    filename = raw_input("Enter filename of training data: ")
    if filename == None or filename == "":
        raise ValueError("Invalid filename.")

    test_filename = raw_input("Enter filename of test data: ")
    if test_filename == None:
        raise ValueError("Invalid filename.")
    
    output_filename = raw_input("Enter output filename: ")
    if output_filename == None:
        raise ValueError("Invalid filename.")
    
    return filename,test_filename, output_filename

def targetClass(target_class_list):
    """
        This function asks user to input target class from the list
        of attributes.
        
        Parameter:
            target_class_list: (list) list of target class.
        
        Return:
            (string): target attribute 
    """
    for i in range(len(target_class_list)):
        print("{0}    {1}".format(i+1,target_class_list[i]))
    target = raw_input("Enter target # of class/attribute: ")
    try:
        if target is None or target == "":
            raise ValueError("Invalid target class/attribute.")
        target = int(target)
        if target < 1 and target > len(target_class_list):
            raise ValueError("Please enter integer for target class in given range.")
    except Exception as exp:
        print(exp)
    return target_class_list[target-1]